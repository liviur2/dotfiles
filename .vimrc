set relativenumber
set nu
set ruler
set autoindent
set showmatch           " Show matching brackets.

set backupdir=~/.vim/backups    " change the place where the backups are stored

syntax enable
set encoding=utf-8
set showcmd                     " display incomplete commands
filetype plugin indent on       " load file type plugins + indentation

"" Whitespace
set nowrap                      " don't wrap lines
set tabstop=2 shiftwidth=2      " a tab is two spaces (or set this to 4)
set expandtab                   " use spaces, not tabs (optional)
set backspace=indent,eol,start  " backspace through everything in insert mode

"" Searching
set hlsearch                    " highlight matches
set incsearch                   " incremental searching
set ignorecase                  " searches are case insensitive...
set smartcase                   " ... unless they contain at least one capital letter
set hidden              " Hide buffers when they are abandoned

"" leader 
let mapleader = ","
imap jk <ESC>

"" Colors
colorscheme monokai 
set background=dark
"" Highlight 80ch
highlight OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
match OverLength /\%81v.\+/

"" Move lines
nnoremap <C-S-j> :m .+1<CR>==
nnoremap <C-S-k> :m .-2<CR>==
inoremap <C-S-j> <ESC>:m .+1<CR>==gi
inoremap <C-S-k> <ESC>:m .-1<CR>==gi
vnoremap <C-S-j> :m '>+1<CR>gv=gv
inoremap <C-S-j> <ESC>:m .+1<CR>==gi
inoremap <C-S-k> <ESC>:m .-1<CR>==gi
vnoremap <C-S-j> :m '>+1<CR>gv=gv
vnoremap <C-S-k> :m '<-2<CR>gv=gv

"" python powerline
"set laststatus=2
"python from powerline.vim import setup as powerline_setup
"python powerline_setup()
"python del powerline_setup
""set rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/

if has("autocmd")
  filetype plugin indent on
endif

"
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
augroup ProjectDrawer
  autocmd!
  autocmd VimEnter * :Vexplore
augroup END

"" Folding
set fdc=1
set fen
set fdm=indent
au BufWinLeave \* mkview
au BufWinEnter \* silent loadview
