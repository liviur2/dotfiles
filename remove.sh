#!/bin/bash

echo "Unlinking files"
unlink ~/.vimrc
unlink ~/.vim/colors/monokai.vim
unlink ~/.tmux.conf

echo "Removing files"
rm -rf ~/.vim
echo "DONE."
