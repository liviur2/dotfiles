#!/bin/bash

# make the dirs :)
echo "Creating dirs"
mkdir -p ~/.vim/colors
mkdir -p ~/.vim/backups

echo "Linking files"
ln .vimrc ~/.vimrc
ln monokai.vim ~/.vim/colors/
ln .tmux.conf ~/.tmux.conf
echo "DONE."
